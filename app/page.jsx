export default function Home() {
    return (
        <section className="py-24">
            <div className="container">
                <h1 className="text-3xl font-bold">NextJS 14 Starter Kit with TailwindCSS on Docker!</h1>
            </div>
        </section>
    );
}

"use client";

import {useEffect} from "react";

const Error = ({error, reset}) => {
    useEffect(() => {
        console.log("Error: ", error);
    }, [error]);

    return (
        <div>
            <h2 className="text-sm text-red-400">Something went wrong! (from app)</h2>
        </div>
    );
};

export default Error;

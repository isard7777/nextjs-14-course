"use client";

import Link from "next/link";
import {usePathname} from "next/navigation";
import React from "react";

const NavLink = ({href, ...rest}) => {
    const path_name = usePathname();
    const is_active = href === path_name;

    return (
        <Link
            className={is_active ? "text-cyan-500" : ""}
            href={href}
            {...rest}
        />
    );
};

export default NavLink;

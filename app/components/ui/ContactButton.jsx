"use client";

import React from "react";
import {useRouter} from "next/navigation";

const ContactButton = () => {
    const router = useRouter();
    function handleClick() {
        //use router to navigate to contact page
        router.push("/contact");
    }

    return <button onClick={handleClick}>Contact</button>;
};

export default ContactButton;

import React from "react";

const About = ({children}) => {
    return (
        <div>
            <h1>About Page</h1>
            <p>Welcome to the About page of our app!</p>
            {children}
        </div>
    );
};

export default About;

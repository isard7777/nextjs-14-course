async function getData() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const random = Math.random();
            if (random > 0.5) {
                resolve("success");
            } else {
                reject("Failed to fetch data");
            }
        }, 2000);
    });
}

const Page = async () => {
    await getData();
    return (
        <div>
            <h1>About</h1>
        </div>
    );
};

export default Page;

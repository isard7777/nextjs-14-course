"use client";

import {useEffect} from "react";

const Error = ({error, reset}) => {
    useEffect(() => {
        console.log(error);
    }, [error]);

    return (
        <div>
            <h2 className="text-sm text-red-400">{error.message}</h2>
            {/*<button
                className="rounded-full bg-red-500 px-4 py-2 text-sm font-bold text-white hover:bg-red-800"
                onClick={() => reset()}
            >
                Try Again
    </button>*/}
        </div>
    );
};

export default Error;

import fs from "fs";
import path from "path";

import {compileMDX} from "next-mdx-remote/rsc";
import Newsletter from "@/app/components/Newsletter";

import rehypePrettyCode from "rehype-pretty-code";
const prettyCodeOptions = {
    theme: "one-dark-pro",
    onVisitLine(node) {
        if (node.children.length === 0) {
            node.children = [{type: "text", value: " "}];
        }
    },
    onVisitHighlightedLine(node) {
        node.properties.className.push("highlighted");
    },
    onVisitHighlightedWord(node) {
        node.properties.className = ["highlighted", "word"];
    }
};

const root_directory = path.join(process.cwd(), "content");

const components = {
    h1: (props) => (
        <h1
            {...props}
            className="text-emerald-400"
        >
            {props.children}
        </h1>
    ),
    Newsletter: Newsletter
};

export async function getPostBySlug(slug) {
    const real_slug = slug.replace(/\.mdx$/, "");

    const file_path = path.join(root_directory, `${real_slug}.mdx`);
    const file_content = fs.readFileSync(file_path, {encoding: "utf8"});
    const {content, frontmatter} = await compileMDX({
        source: file_content,
        components,
        options: {
            parseFrontmatter: true,
            mdxOptions: {
                rehypePlugins: [[rehypePrettyCode, prettyCodeOptions]]
            }
        }
    });

    await wait(2000);

    return {
        content,
        frontmatter,
        slug: real_slug
    };
}

export async function getAllPosts() {
    const files = fs.readdirSync(root_directory);

    let posts = [];

    for (const file of files) {
        const post = await getPostBySlug(file);

        posts.push(post);
    }

    await wait(2000);
    return posts;
}

export async function wait(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

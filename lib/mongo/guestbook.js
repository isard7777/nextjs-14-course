import clientPromise from "@/lib/mongo/clients";
import {cache} from "react";
let client;
let db;
let guestbook;

async function init() {
    if (db) return;

    try {
        client = await clientPromise;

        db = await client.db("nextjs-course");
        guestbook = await db.collection("guestbook");
    } catch (error) {
        throw new Error("Fail to connect to database");
    }
}

(async () => {
    await init();
})();

export const getGuestbookEntries = cache(async () => {
    try {
        if (!guestbook) await init();

        console.log("fetching entries....");

        const entries = await guestbook
            .find({})
            .map((entry) => ({...entry, _id: entry._id.toString()}))
            .toArray();

        return {entries};
    } catch (error) {
        return {error: "Failed to fetch guestbook entries"};
    }
});

export const createGuestbookEntry = async ({name, message}) => {
    try {
        if (!guestbook) await init();

        return await guestbook.insertOne({name, message, updateAt: new Date()});
    } catch (error) {
        return {error: "Failed to create guestbook entry"};
    }
};
